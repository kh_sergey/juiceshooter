using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour, ISelectable
{
    [SerializeField] private float _timeToReachTarget;
    //[SerializeField] private float _timeToCreateCoin;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _heightOnPickUp;
    [SerializeField] private float _autoPickUpDistance;

    private GameObject _player;
    private GameObject _coinIcon;
    private TrailRenderer _trailRenderer;
    private Sequence _sequence;
    private Collider _collider;
    private PlayerCoins _playerCoins;

    private int _value;
    private float _coinUIToWorldPointDistance = 2f;
    private bool _rotate = true;

    private void Awake()
    {
        _trailRenderer = GetComponent<TrailRenderer>();
        _collider = GetComponent<Collider>();
        _coinIcon = FindObjectOfType<CoinsCounterUI>().CoinIcon;
        _player = FindObjectOfType<PlayerHitReaction>().gameObject;
        _playerCoins = FindObjectOfType<PlayerCoins>();
    }

    private void Start()
    {
        // ��������� ��� ����������. ���� ���������� ������ � ������ - ����� ���������
        PlayCreateAnimation();

        float distance = Vector3.Distance(transform.position, _player.transform.position);

        if (distance <= _autoPickUpDistance)
        {
            PickUp();
        }
    }

    private void PlayCreateAnimation()
    {
        // �������� �������� �� ����������� ���������
        
        /*
        _sequence = DOTween.Sequence();
        _sequence.Append(transform.DOMove(new Vector3(transform.position.x, transform.position.y, transform.position.z), _timeToCreateCoin / 2));
        _sequence.Insert(0, transform.DOScale(new Vector3(0.2f, 0.2f, 0.2f), _timeToCreateCoin / 2));
        */
        //_sequence.Join(transform.DOMove(screenToWorldPosition, _timeToCreateCoin / 2));
    }

    private void Update()
    {
        if (_rotate)
        {
            transform.Rotate(0, _rotationSpeed, 0, Space.World);
        }
    }

    public void Select()
    {
        PickUp();
    }

    public void SetValue(int value)
    {
        _value = value;
    }

    public void PickUp()
    {
        _rotate = false;
        _trailRenderer.enabled = true;
        _collider.enabled = false;
        PlayPickUpAnimation();
    }

    private void PlayPickUpAnimation()
    {
        // �������� ������� ���������� � �������� � ���� ��������� DOMove, ����� ��� ������ ���������� ������� ������ � ���� �����
        // ���� ��������� ���������� ������

        Vector3 screenToWorldPosition = Camera.main.ScreenToWorldPoint(new Vector3(_coinIcon.transform.position.x, _coinIcon.transform.position.y, _coinUIToWorldPointDistance));

        _sequence = DOTween.Sequence();
        _sequence.Append(transform.DOMove(new Vector3(transform.position.x, transform.position.y + _heightOnPickUp, transform.position.z), _timeToReachTarget/2));
        _sequence.Insert(0, transform.DOScale(new Vector3(0.05f, 0.05f, 0.05f), _timeToReachTarget/2));
        _sequence.Join(transform.DOMove(screenToWorldPosition, _timeToReachTarget/2));

        _sequence.OnComplete(() => IncreasePlayerCoinValue());
    }

    private void IncreasePlayerCoinValue()
    {
        _sequence.Kill();

        _playerCoins.Increase(_value);

        //Destroy(gameObject); // �� trailRenderer ���� autodestroy
    }
}
