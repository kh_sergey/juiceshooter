using UnityEngine;
using UnityEngine.Events;

public class Border : MonoBehaviour, ISelectable
{
    public event UnityAction OnBorderSelected;

    public void Select()
    {
        OnBorderSelected?.Invoke();
    }
}
