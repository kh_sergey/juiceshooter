using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _coinPrefab;
    [SerializeField] private int _value = 1;

    private Transform _spawnedCoinParent;

    public void SetCoinParent(Transform parent)
    {
        _spawnedCoinParent = parent;
    }

    public void Spawn()
    {
        GameObject spawnedCoin = Instantiate(_coinPrefab, new Vector3(transform.position.x, _coinPrefab.transform.position.y, transform.position.z), Quaternion.identity); // ��� ���������?
        spawnedCoin.GetComponent<Coin>().SetValue(_value);
        spawnedCoin.transform.parent = _spawnedCoinParent;
    }
}
