using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private const string _main = "Main"; // ����� ����� � ��� �����?
    private const string _menu = "Menu";

    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadMainScene()
    {
        SceneManager.LoadScene(_main);
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene(_menu);
    }
}
