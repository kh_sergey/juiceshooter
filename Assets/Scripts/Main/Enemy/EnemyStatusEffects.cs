using System.Collections;
using UnityEngine;

public class EnemyStatusEffects : MonoBehaviour
{
    public Task SlowMovementProcessTask { get; private set; }
    public Task DamagePerPeriodProcessTask { get; private set; }

    [SerializeField] private GameObject _slowMovementParticles;
    [SerializeField] private GameObject _damagerPerSecondParticles;

    private EnemyMovement _enemyMovement;
    private EnemyHealth _enemyHealth;
    private AnimatorEnemy _animatorEnemy;

    private float _originalMovementSpeed;
    private float _originalAnimationSpeed;

    private void Awake()
    {
        _enemyMovement = GetComponent<EnemyMovement>();
        _enemyHealth = GetComponent<EnemyHealth>();
        _animatorEnemy = GetComponent<AnimatorEnemy>();
    }

    private void Start()
    {
        _originalMovementSpeed = _enemyMovement.Agent.speed;
        _originalAnimationSpeed = _animatorEnemy.Speed;
    }

    public void SlowMovement(float slowModifier, float time)
    {
        if (SlowMovementProcessTask != null)
        {
            SlowMovementProcessTask.Stop();
        }

        if (_enemyHealth.Alive)
        {
            SlowMovementProcessTask = new Task(SlowMovementProcess(slowModifier, time));
        }
    }

    private IEnumerator SlowMovementProcess(float slowModifier, float time)
    {
        _enemyMovement.SetSpeed(_originalMovementSpeed * (1 - slowModifier));
        _animatorEnemy.Speed = _originalAnimationSpeed * (1 - slowModifier);
        _slowMovementParticles.SetActive(true);

        yield return new WaitForSeconds(time);

        if (_enemyHealth.Alive)
        {
            _enemyMovement.SetSpeed(_originalMovementSpeed);
            _animatorEnemy.Speed = _originalAnimationSpeed;
            _slowMovementParticles.SetActive(false);
        }
    }

    public void DamagePerPeriod(float damage, float effectTime, float timePeriod)
    {
        if (DamagePerPeriodProcessTask != null)
        {
            DamagePerPeriodProcessTask.Stop();
        }

        if (_enemyHealth.Alive)
        {
            DamagePerPeriodProcessTask = new Task(DamagePerPeriodProcess(damage, effectTime, timePeriod));
        }
    }

    private IEnumerator DamagePerPeriodProcess(float damage, float effectTime, float timePeriod)
    {
        float ellapsedTime = 0f;

        _damagerPerSecondParticles.SetActive(true);

        while (ellapsedTime < effectTime)
        {
            _enemyHealth.Damage(damage, false);

            yield return new WaitForSeconds(timePeriod);

            ellapsedTime += timePeriod;
        }

        if (_enemyHealth.Alive)
        {
            _damagerPerSecondParticles.SetActive(false);
        }
    }

    public void ClearVisualEffects()
    {
        _slowMovementParticles.SetActive(false);
        _damagerPerSecondParticles.SetActive(false);
    }
}
