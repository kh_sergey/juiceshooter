using UnityEngine;
using UnityEngine.Events;

public class EnemyCounter : MonoBehaviour
{
    public event UnityAction OnCountNull;
    public event UnityAction OnCountChanged;

    public int Count { get => _count; private set { _count = value; OnCountChanged?.Invoke(); } }

    private int _count;

    private EnemySpawner _enemySpawner;
    private EnemyEventManager _enemyEventManager;

    private void Awake()
    {
        _enemySpawner = GetComponent<EnemySpawner>();
        _enemyEventManager = GetComponent<EnemyEventManager>();
    }

    private void OnEnable()
    {
        _enemySpawner.OnStartSpawnProcess += SetCurrentEnemyCount;
        _enemyEventManager.OnEnemyDied += DecreaseCurrentEnemyCount;
    }

    private void OnDisable()
    {
        _enemySpawner.OnStartSpawnProcess -= SetCurrentEnemyCount;
        _enemyEventManager.OnEnemyDied -= DecreaseCurrentEnemyCount;
    }

    private void SetCurrentEnemyCount(int count)
    {
        Count = count;
    }

    private void DecreaseCurrentEnemyCount()
    {
        Count--;

        if (Count <= 0)
        {
            OnCountNull?.Invoke();
        }
    }
}
