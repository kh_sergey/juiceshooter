using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Linq;

[Serializable]
public class PositionsSet
{
    public Vector3 SpawnPosition;
    public Vector3 DestinationPosition;
}

public class EnemySpawner : MonoBehaviour
{
    public event UnityAction<int> OnStartSpawnProcess;

    [SerializeField] private List<PositionsSet> _positionsSets;
    [SerializeField] private Transform _spawnedEnemyParent;
    [SerializeField] private Transform _spawnedCoinParent; // ��������� ������������ ������ ��� ������� ��� ������. ������� ����� ������ ��� ���.
    [SerializeField] private float _randomSpawnRadius = 0.8f;

    private Task _spawnProcess;

    public void StartSpawn(SpawnQueue queue)
    {
        if (_spawnProcess != null)
        {
            _spawnProcess.Stop();
        }

        int numberOfSpawns = queue.NumberOfSpawns.Sum();
        OnStartSpawnProcess?.Invoke(numberOfSpawns);
        _spawnProcess = new Task(SpawnProcess(queue));
    }

    private IEnumerator SpawnProcess(SpawnQueue queue) // ����� �� ���� ���������� ���� ��� �������� positionsSet?
    {                                                                                                               // ����� ����� � ��� �����?
        List<PositionsSet> tmpPositionsSets = new List<PositionsSet>(_positionsSets);
        PositionsSet positionsSet = null;

        for (int i = 0; i < queue.EnemyTypes.Count; i++)
        {
            for (int j = queue.NumberOfSpawns[i]; j > 0; j--)
            {
                if (tmpPositionsSets.Count == 0)
                {
                    tmpPositionsSets = new List<PositionsSet>(_positionsSets);
                }

                int randomNumber = UnityEngine.Random.Range(0, tmpPositionsSets.Count);
                positionsSet = tmpPositionsSets[randomNumber];
                tmpPositionsSets.RemoveAt(randomNumber);

                GameObject spawnedEnemyObject = Instantiate(
                    queue.EnemyTypes[i], new Vector3(
                        positionsSet.SpawnPosition.x + UnityEngine.Random.Range(0, _randomSpawnRadius),
                        positionsSet.SpawnPosition.y,
                        positionsSet.SpawnPosition.z + UnityEngine.Random.Range(0, _randomSpawnRadius)),
                    Quaternion.Euler(0, 180f, 0));

                spawnedEnemyObject.transform.parent = _spawnedEnemyParent;
                spawnedEnemyObject.GetComponent<CoinSpawner>().SetCoinParent(_spawnedCoinParent);
                EnemyMovement spawnedEnemy = spawnedEnemyObject.GetComponent<EnemyMovement>();
                spawnedEnemy.SetDestinationForStart(positionsSet.DestinationPosition);

                yield return new WaitForSeconds(queue.SpawnCooldown);
            }
        }
    }
}
