using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public NavMeshAgent Agent { get; private set; }

    private Vector3 _destination;

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        Agent.enabled = false;
    }

    public void SetDestinationForStart(Vector3 destinationPoint)
    {
        _destination = destinationPoint;
    }

    public void StartMovement()
    {
        Agent.enabled = true;
        Agent.SetDestination(_destination);
    }

    public void StopMovement()
    {
        Agent.enabled = false;
    }

    public void SetSpeed(float speed)
    {
        Agent.speed = speed;
    }
}
