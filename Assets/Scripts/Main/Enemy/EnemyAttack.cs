using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] private float _attackDistance = 1.6f;

    private GameObject _player;
    private AnimatorEnemy _animatorEnemy;
    private EnemyMovement _enemyMovement;
    private PlayerHitReaction _playerHitReaction;

    private void Awake()
    {
        _animatorEnemy = GetComponent<AnimatorEnemy>();
        _playerHitReaction = FindObjectOfType<PlayerHitReaction>();
        _player = _playerHitReaction.gameObject;
        _enemyMovement = GetComponent<EnemyMovement>();
    }

    private void FixedUpdate()
    {
        CheckAttackDistance();
    }

    private void CheckAttackDistance()
    {
        float distance = Vector3.Distance(transform.position, _player.transform.position);

        if (distance <= _attackDistance)
        {
            StartAttack();
        }
    }

    private void StartAttack()
    {
        _enemyMovement.StopMovement();
        _animatorEnemy.SetState(AnimatorEnemy.AnimatorStates.Attack);
    }

    public void KillPlayer()
    {
        _playerHitReaction.Die();
    }
}
