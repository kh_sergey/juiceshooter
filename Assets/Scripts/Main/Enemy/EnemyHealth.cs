using UnityEngine;
using UnityEngine.Events;
using LiquidVolumeFX;

public class EnemyHealth : MonoBehaviour // �� ����������� �� LiquidCapacity, ������ ���������� �������
{
    [SerializeField] private UnityEvent _died = new UnityEvent();

    public bool Alive { get; private set; } = true;

    public float MaxHealth { get => _maxHealth; private set => _maxHealth = value; }
    public float Health { 
        get => _health;
        private set { 
            if (value != _health) 
            {
                _health = Mathf.Clamp(value, 0, MaxHealth);
                UpdateHealthVisuals(_health);
            } 
        }
    }

    [SerializeField] private float _maxHealth = 100f;
    [SerializeField] private float _liquidLerpSpeed = 5f;

    private LiquidVolume _liquidVolume;
    private EnemyPointer _pointer;
    private AnimatorEnemy _animatorEnemy;
    private EnemyStagger _enemyStagger;
    private EnemyStatusEffects _enemyStatusEffects;
    private EnemyEventManager _enemyEventManager;
    private EnemySelect _enemy;

    private float _health;
    private float _currentLiquidLevel;
    private float _targetLiquidLevel;
    private float _maxLiquidLevel = 0.7f; // 0% hp
    private float _minLiquidLevel = 0.1f; // 100% hp

    private void Awake()
    {
        _enemyStagger = GetComponent<EnemyStagger>();
        _enemyStatusEffects = GetComponent<EnemyStatusEffects>();
        _pointer = FindObjectOfType<EnemyPointer>();
        _animatorEnemy = GetComponentInChildren<AnimatorEnemy>();
        _liquidVolume = GetComponentInChildren<LiquidVolume>();
        _enemy = GetComponent<EnemySelect>();
        Health = MaxHealth;
    }

    private void Start()
    {
        _enemyEventManager = GetComponentInParent<EnemyEventManager>();
    }

    private void Update()
    {
        if (_currentLiquidLevel != _targetLiquidLevel)
        {
            _currentLiquidLevel = Mathf.Lerp(_liquidVolume.level, _targetLiquidLevel, _liquidLerpSpeed * Time.deltaTime);
            _liquidVolume.level = _currentLiquidLevel;
        }
    }

    private void Die()
    {
        if (Alive)
        {
            Alive = false;
            _died.Invoke(); // ��� ���������� ������ ���������� ��������. ��� �����? ����� ���������?
            _enemyEventManager.Die();
            _enemyStagger.StaggerProcessTask?.Stop();
            _animatorEnemy.SetState(AnimatorEnemy.AnimatorStates.Fall);

            if (_pointer.PointedEnemy == _enemy.ObjectToAim)
            {
                _pointer.Clear();
            }
        }
    }

    public void Damage(float damage, bool stagger)
    {
        Health -= damage;

        if (Health <= 0)
        {
            Die();
        }

        if (stagger)
        {
            _enemyStagger.StartStagger();
        }
    }

    private void UpdateHealthVisuals(float healthValue)
    {
        _targetLiquidLevel = (_minLiquidLevel - _maxLiquidLevel) / _maxHealth * healthValue + _maxLiquidLevel;
    }

    private void OnDestroy()
    {
        if (gameObject.scene.isLoaded) // ����� �� �������� ������ ����� ���������� ���� � �������
        {
            _enemyStagger.StaggerProcessTask?.Stop();
            _enemyStatusEffects.SlowMovementProcessTask?.Stop();
            _enemyStatusEffects.DamagePerPeriodProcessTask?.Stop();
        }
    }
}