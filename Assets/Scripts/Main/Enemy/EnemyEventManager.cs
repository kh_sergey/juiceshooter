using UnityEngine;
using UnityEngine.Events;
using System;

public class EnemyEventManager : MonoBehaviour
{
    // ��������� ����� ��� ������� ����������. ������ ����� � ������ ����������
    // � �������������/������������ �� ���� ������� ��������.

    public event UnityAction<GameObject> OnEnemySelected;
    public event UnityAction OnEnemyDied;

    public void Select(GameObject enemy)
    {
        OnEnemySelected?.Invoke(enemy);
    }

    public void Die()
    {
        OnEnemyDied?.Invoke();
    }
}
