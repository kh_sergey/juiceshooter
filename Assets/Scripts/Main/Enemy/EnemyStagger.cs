using System.Collections;
using UnityEngine;

public class EnemyStagger : MonoBehaviour
{
    public Task StaggerProcessTask { get; private set; }

    [SerializeField] private float _staggerTime = 0.4f;

    private EnemyHealth _enemyHealth;
    private EnemyMovement _enemyMovement;
    private AnimatorEnemy _animator;

    private bool _staggered = false; // �������������� ������ �� ������ ������������ ��������. �������.
                                     // _animator.GetCurrentAnimatorStateInfo(0) ���������� ������� ��������� � ���������.

    private void Awake()
    {
        _enemyHealth = GetComponent<EnemyHealth>();
        _enemyMovement = GetComponent<EnemyMovement>();
        _animator = GetComponent<AnimatorEnemy>();
    }

    public void StartStagger()
    {
        if (StaggerProcessTask != null)
        {
            StaggerProcessTask.Stop();
        }

        if (_enemyHealth.Alive)
        {
            StaggerProcessTask = new Task(StaggerProcess());
        }
    }

    private IEnumerator StaggerProcess()
    {
        SetStaggerState(true);

        yield return new WaitForSeconds(_staggerTime);

        SetStaggerState(false);
    }

    private void SetStaggerState(bool state)
    {
        if (state == true && !_staggered)
        {
            _staggered = true;
            _enemyMovement.StopMovement();
            _animator.SetState(AnimatorEnemy.AnimatorStates.Stagger);
        }

        if (state == false)
        {
            _enemyMovement.StartMovement();
            _animator.SetState(AnimatorEnemy.AnimatorStates.Walk);
            _staggered = false;
        }
    }
}
