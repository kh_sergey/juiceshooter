using UnityEngine;

public class AnimatorEnemy : MonoBehaviour
{
    private const string ANIMATOR_RUN_SPEED = "RunSpeed";

    public enum AnimatorStates { Fall, Stagger, Walk, Attack };

    public float Speed { 
        get => _animator.GetFloat(ANIMATOR_RUN_SPEED);
        set => _animator.SetFloat(ANIMATOR_RUN_SPEED, value);
    }

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    public void SetState(AnimatorStates state)
    {
        _animator.SetTrigger(state.ToString());
    }
}
