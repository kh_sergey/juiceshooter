using UnityEngine;

public class EnemySelect : MonoBehaviour, ISelectable
{
    public GameObject ObjectToAim { get => _objectToAim; }
                                                           
    [SerializeField] private GameObject _objectToAim;

    private EnemyEventManager _eventManager;

    private void Start()
    {
        _eventManager = GetComponentInParent<EnemyEventManager>();
    }

    public void Select()
    {
        _eventManager.Select(ObjectToAim);
    }
}
