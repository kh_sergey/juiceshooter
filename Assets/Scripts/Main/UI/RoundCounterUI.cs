using UnityEngine;
using TMPro;

public class RoundCounterUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _roundsCounterText;

    private string text;

    private RoundController _roundController;

    private void Awake()
    {
        _roundController = FindObjectOfType<RoundController>();
        text = _roundsCounterText.text;
    }

    private void OnEnable()
    {
        _roundController.OnRoundChanged += UpdateCanvasInfo;
    }

    private void OnDisable()
    {
        _roundController.OnRoundChanged -= UpdateCanvasInfo;
    }

    private void UpdateCanvasInfo()
    {
        _roundsCounterText.text = text + _roundController.Round.ToString();
    }
}
