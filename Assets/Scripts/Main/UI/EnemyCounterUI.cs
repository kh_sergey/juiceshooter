using UnityEngine;
using TMPro;

public class EnemyCounterUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _counterText;

    private EnemyCounter _enemyCounter;

    private void Awake()
    {
        _enemyCounter = FindObjectOfType<EnemyCounter>();
    }

    private void OnEnable()
    {
        _enemyCounter.OnCountChanged += UpdateCanvasInfo;
    }

    private void OnDisable()
    {
        _enemyCounter.OnCountChanged -= UpdateCanvasInfo;
    }

    private void UpdateCanvasInfo()
    {
        _counterText.text = _enemyCounter.Count.ToString();
    }
}
