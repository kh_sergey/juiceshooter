using UnityEngine;
using TMPro;

public class CoinsCounterUI : MonoBehaviour
{
    public GameObject CoinIcon { get => _coinIcon; private set => _coinIcon = value; }

    [SerializeField] private TextMeshProUGUI _coinsText;
    [SerializeField] private GameObject _coinIcon;
    
    private PlayerCoins _playerCoins;

    private void Awake()
    {
        _playerCoins = FindObjectOfType<PlayerCoins>();
    }

    private void OnEnable()
    {
        _playerCoins.OnCoinsValueChanged += UpdateCanvasInfo;
    }

    private void OnDisable()
    {
        _playerCoins.OnCoinsValueChanged -= UpdateCanvasInfo;
    }

    private void UpdateCanvasInfo()
    {
        // ��� ���������� ���������� ������� - ������� ������� ������ ������������� � �������� (����������� ��������)

        _coinsText.text = _playerCoins.Value.ToString();
    }
}
