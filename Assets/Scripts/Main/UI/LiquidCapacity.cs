using UnityEngine;
using LiquidVolumeFX;

public abstract class LiquidCapacity : MonoBehaviour
{
    public float MaxValue { get => _maxValue; private set => _maxValue = value; }
    public virtual float Value
    {
        get => _value;
        protected set
        {
            _value = Mathf.Clamp(value, 0, MaxValue);
            UpdateJuiceVisuals(_value);
        }
    }

    [SerializeField] protected float _maxValue = 100f;
    [SerializeField] protected float _liquidLerpSpeed = 2f;
    [SerializeField] protected float _maxLiquidLevel;
    [SerializeField] protected float _minLiquidLevel;

    [SerializeField] protected LiquidVolume _liquidVolume;

    protected float _value;
    protected float _currentLiquidLevel;
    protected float _targetLiquidLevel;

    protected virtual void Start()
    {
        Value = 0f;
        _liquidVolume.level = 0f;
    }

    protected virtual void Update() // ������ ������ ������
    {
        if (_currentLiquidLevel != _targetLiquidLevel)
        {
            _currentLiquidLevel = Mathf.Lerp(_liquidVolume.level, _targetLiquidLevel, _liquidLerpSpeed * Time.deltaTime);
            _liquidVolume.level = _currentLiquidLevel;
        }
    }

    public virtual void Increase(float increaseValue)
    {
        Value += increaseValue;
    }

    public virtual void Decrease(float decreaseValue)
    {
        Value -= decreaseValue;
    }

    protected virtual void UpdateJuiceVisuals(float juiceValue)
    {
        _targetLiquidLevel = (_maxLiquidLevel - _minLiquidLevel) / _maxValue * juiceValue + _minLiquidLevel;
    }
}
