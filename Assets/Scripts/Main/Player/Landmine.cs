using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Landmine : MonoBehaviour
{
    [SerializeField] private UnityEvent _expoloded;

    public float ExplodeRadius { get => _explodeRadius; } // ��� dummyLandmine � MinesShooter
    public bool IsActivated { get; private set; }

    [SerializeField] private float _activationTime = 2f;
    [SerializeField] private float _damage = 30f;
    [SerializeField] private float _explodeRadius = 0.75f;
    [SerializeField][Range(0,1)] private float _slowMovementModifier = 0.2f; // %
    [SerializeField] private float _slowMovementTime = 2.5f;

    [SerializeField] private GameObject _radiusObject;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private LayerMask _enemyLayerMask;

    private void Start()
    {
        ApplyExplodeRadius();
        new Task(Activate());
    }

    private void ApplyExplodeRadius()
    {
        _radiusObject.transform.localScale = new Vector3(_explodeRadius, _explodeRadius, _explodeRadius);
    }

    private IEnumerator Activate()
    {
        yield return new WaitForSeconds(_activationTime);

        _radiusObject.SetActive(true);
        IsActivated = true;
    }

    public void Explode()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, _explodeRadius, _enemyLayerMask);

        foreach(Collider hitCollider in hitColliders)
        {
            Damage(hitCollider.GetComponent<EnemyHealth>());
        }

        _expoloded.Invoke();
        Destroy(gameObject, _particleSystem.main.duration);
    }

    private void Damage(EnemyHealth enemy)
    {
        enemy.Damage(_damage, false);
        EnemyStatusEffects enemyStatusEffects = enemy.GetComponent<EnemyStatusEffects>();
        enemyStatusEffects.SlowMovement(_slowMovementModifier, _slowMovementTime);
    }
}
