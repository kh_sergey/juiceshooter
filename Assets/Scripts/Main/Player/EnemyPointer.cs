using UnityEngine;
using UnityEngine.Events;

public class EnemyPointer : MonoBehaviour
{
    public event UnityAction OnPointSet;
    public event UnityAction OnPointCleared;

    public GameObject PointedEnemy { get => _pointedEnemy; private set { _pointedEnemy = value; } }

    private GameObject _pointedEnemy;

    public void Set(GameObject enemy)
    {
        if (PointedEnemy != null)
        {
            PointedEnemy.GetComponentInParent<Outline>().enabled = false;
        }

        PointedEnemy = enemy;
        OnPointSet.Invoke();
        PointedEnemy.GetComponentInParent<Outline>().enabled = true;
    }

    public void Clear()
    {
        if (PointedEnemy != null)
        {
            PointedEnemy.GetComponentInParent<Outline>().enabled = false;
            PointedEnemy = null;
            OnPointCleared.Invoke();
        }
    }
}
