using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class CuttingBoard : MonoBehaviour, ISelectable
{
    public event UnityAction OnCuttingBoardSelected;

    [SerializeField] private float _juiceGainPerCut = 10f;
    [SerializeField] private float _sizeReductionDuration = 0.5f;

    private Fruit _fruit;
    private GameObject _fullFruit;
    private Vector3 _originalFruitScale;
    private JuiceCapacity _gainableJuice; // ���������� ���
    private AnimatorKnife _knifeAnimator;
    private Sequence _reductSizeSequence;

    private float _juiceLeftAmount;

    private void Awake()
    {
        _knifeAnimator = FindObjectOfType<AnimatorKnife>();
    }

    public void Select()
    {
        OnCuttingBoardSelected?.Invoke();
    }

    public void PutFruitOnTable(Fruit fruit)
    {
        if (_fruit != null)
        {
            RemoveFruit();
        }

        _fruit = fruit;
        _fullFruit = _fruit.FruitModel;
        _gainableJuice = _fruit.GainableJuice;
        _originalFruitScale = _fullFruit.transform.localScale;
        _fullFruit.SetActive(true);
        _juiceLeftAmount = _fruit.JuiceAmount;

        OnCuttingBoardSelected += CutFruit;
    }

    public void CutFruit()
    {
        if (_juiceLeftAmount > _juiceGainPerCut)
        {
            _gainableJuice.Increase(_juiceGainPerCut);
            _juiceLeftAmount -= _juiceGainPerCut;
        } else
        {
            _gainableJuice.Increase(_juiceLeftAmount);
            _juiceLeftAmount = 0;
        }

        _knifeAnimator.MakeCut();
        ReductFruitSize();

        if (_juiceLeftAmount == 0f)
        {
            GetFullCutBonus();
            RemoveFruit();
        }
    }

    private void GetFullCutBonus()
    {
        _fruit.FruitDestroyed.Invoke();
        _fruit.PowerUp.Increase(_fruit.PowerUpIncreaseValue);
        _gainableJuice.Increase(_fruit.FruitDestroyJuiceAmount);
    }

    private void ReductFruitSize()
    {
        if (_reductSizeSequence != null)
        {
            _reductSizeSequence.Kill();
        }

        _reductSizeSequence = DOTween.Sequence();

        _reductSizeSequence.Append(_fullFruit.transform.DOScale(new Vector3(
            _fullFruit.transform.localScale.x - _fruit.SizeReductionPerCut, // ��� ����� ���-�� ���������?
            _fullFruit.transform.localScale.y - _fruit.SizeReductionPerCut,
            _fullFruit.transform.localScale.z - _fruit.SizeReductionPerCut), _sizeReductionDuration));
    }

    public void RemoveFruit()
    {
        _fruit = null;
        _reductSizeSequence.Kill();
        _fullFruit.transform.localScale = _originalFruitScale;
        _fullFruit.SetActive(false);
        _fullFruit = null;
        _originalFruitScale = Vector3.zero;
        _gainableJuice = null;

        OnCuttingBoardSelected -= CutFruit;
    }
}
