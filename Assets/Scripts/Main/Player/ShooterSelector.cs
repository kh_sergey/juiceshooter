using UnityEngine;

public class ShooterSelector : MonoBehaviour
{
    [SerializeField] private Shooter _defaultShooter;

    private Shooter _selectedShooter;

    private void Start()
    {
        _defaultShooter.Select();
    }

    public void SelectShooter(Shooter shooter)
    {
        if (_selectedShooter != null)
        {
            _selectedShooter.Unselect();
        }

        _selectedShooter = shooter;
    }
}
