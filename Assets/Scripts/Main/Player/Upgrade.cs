using UnityEngine;

public abstract class Upgrade : MonoBehaviour
{
    public int Level { get; protected set; }

}
