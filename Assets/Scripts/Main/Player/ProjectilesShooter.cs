using UnityEngine;

public class ProjectilesShooter : Shooter
{
    [SerializeField] private float _projectileSpeed;
    [SerializeField] private float _shootProjectileJuiceCost;

    [SerializeField] private GameObject _projectilePrefab;
    [SerializeField] private Vector3 _spawnProjectilePosition;
    [SerializeField] private LayerMask _shootingLayerMask;

    private JuiceCapacity _redJuice;

    protected override void Awake()
    {
        base.Awake();
        _redJuice = FindObjectOfType<JuiceCapacitiesStorage>().RedJuice;
    }

    public override void Select()
    {
        if (IsUnlocked)
        {
            base.Select();
            _playerInput.OnTouchDown += ShootProjectile;
        }
    }

    public override void Unselect()
    {
        base.Unselect();
        _playerInput.OnTouchDown -= ShootProjectile;
    }

    public override void SetUpgradeLevel()
    {
        throw new System.NotImplementedException();
    }

    /*
    public override void Unlock()
    {
        throw new System.NotImplementedException();
    }
    */

    private void ShootProjectile()
    {
        if (_redJuice.Value >= _shootProjectileJuiceCost)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _shootingLayerMask))
            {
                if (hit.collider.gameObject.GetComponent<Coin>() == null) // ����� ����� ���� ��������� ������� ��� ��������
                {
                    GameObject projectile = Instantiate(_projectilePrefab, _spawnProjectilePosition, Quaternion.identity) as GameObject;
                    projectile.transform.LookAt(hit.point);
                    projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * _projectileSpeed);

                    _redJuice.Decrease(_shootProjectileJuiceCost);
                }
            }
        }
    }
}
