using UnityEngine;

public class SplashRadiusChanger : MonoBehaviour
{
    [SerializeField] private GameObject _projectileSplashObject;

    public void ChangeSplashObjectScale(float radius)
    {
        _projectileSplashObject.transform.localScale = new Vector3(radius, radius, radius);
    }
}
