using UnityEngine;
using UnityEngine.Events;

public class Fruit : MonoBehaviour
{
    public UnityEvent FruitDestroyed = new UnityEvent();

    public JuiceCapacity GainableJuice { get => _gainableJuice; }
    public GameObject FruitModel { get => _fruitModel; }
    public PowerUpCapacity PowerUp { get => _powerUp; }

    public Vector3 HoverOffset { get => _hoverOffset; }
    public float SizeReductionPerCut { get => _sizeReductionPerCut; }
    public float PowerUpIncreaseValue { get => _powerUpIncreaseValue; }
    public float JuiceAmount { get => _juiceAmount; }
    public float FruitDestroyJuiceAmount { get => _fruitDestroyJuiceAmount; }

    [SerializeField] private JuiceCapacity _gainableJuice;
    [SerializeField] private GameObject _fruitModel;
    [SerializeField] private PowerUpCapacity _powerUp; // ���������� �������

    [SerializeField] private Vector3 _hoverOffset;
    [SerializeField] private float _sizeReductionPerCut;
    [SerializeField] private float _powerUpIncreaseValue;
    [SerializeField] private float _juiceAmount;
    [SerializeField] private float _fruitDestroyJuiceAmount; // ���������� ���� ����� �����������
}
