using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class SpawnQueue
{
    public List<GameObject> EnemyTypes = new List<GameObject>();
    public List<int> NumberOfSpawns = new List<int>();
    public float SpawnCooldown;

    public SpawnQueue(RoundController _roundInfo)
    {
        // ������� ���������� ����� ���� ����������� ������������
        float index = (float)_roundInfo.Round / _roundInfo.EnemyTypeCycle;
        if (index < _roundInfo.EnemyTypesPrefabs.Count && index - 1 >= 0)
        {
            EnemyTypes.Add(_roundInfo.EnemyTypesPrefabs[(int)index - 1]);
        }
        if (index < _roundInfo.EnemyTypesPrefabs.Count)
        {
            EnemyTypes.Add(_roundInfo.EnemyTypesPrefabs[(int)index]);
        }
        if (index >= _roundInfo.EnemyTypesPrefabs.Count)
        {
            EnemyTypes.Add(_roundInfo.EnemyTypesPrefabs[_roundInfo.EnemyTypesPrefabs.Count - 1]);
        }

        // ����� ��������� ���������� ����������� ����� �����������
        List<float> percents = new List<float>();
        if (EnemyTypes.Count > 1)
        {
            percents.Add(1 - (index - (int)index));
            percents.Add(index - (int)index);
        }
        else
        {
            percents.Add(1);
        }

        // ����� � ����������� �� ���������� ����������� ���������� ������� � ���������� ���
        for (int i = 0; i < EnemyTypes.Count; i++)
        {
            NumberOfSpawns.Add(CalculateEnemyCount(_roundInfo.EnemyCount, percents[i]));
        }

        SpawnCooldown = CalculateSpawnCooldown(_roundInfo.SpawnCooldownRange, _roundInfo.Round, _roundInfo.SpawnCooldownDecrement);
    }

    private int CalculateEnemyCount(int enemyCount, float percent)
    {
        return Mathf.RoundToInt(enemyCount * percent);
    }

    private float CalculateSpawnCooldown(Vector2 cooldownRange, int round, float cooldownDecrement)
    {
        return cooldownRange.y - (round * cooldownDecrement);
    }
}

public class RoundController : MonoBehaviour
{
    public event UnityAction OnRoundChanged;

    public int Round { get => _round; private set { _round = value; OnRoundChanged?.Invoke(); } }
    public int EnemyTypeCycle { get => _enemyTypeCycle; private set => _enemyTypeCycle = value; }
    public List<GameObject> EnemyTypesPrefabs { get => _enemyTypesPrefabs; private set => _enemyTypesPrefabs = value; }
    public int EnemyCount { get => _enemyCount; private set => _enemyCount = value; }
    public Vector2 SpawnCooldownRange { get => _spawnCooldownRange; private set => _spawnCooldownRange = value; }
    public float SpawnCooldownDecrement { get => _spawnCooldownDecrement; private set => _spawnCooldownDecrement = value; }

    [SerializeField] private List<GameObject> _enemyTypesPrefabs;
    [SerializeField] private int _enemyCountIncrement = 5;
    [SerializeField] private Vector2 _spawnCooldownRange = new Vector2(2f, 3.5f); // min max
    [SerializeField] private float _spawnCooldownDecrement = 0.075f;
    [SerializeField] private float _roundStartCooldown = 7f;
    [SerializeField] private int _enemyCount = 10;
    [SerializeField] private int _enemyTypeCycle = 5; // ����� ��� ����������� ���������� ������ _enemyTypeCycle(5) �������

    private int _round;

    private EnemySpawner _enemySpawner;
    private EnemyCounter _enemyCounter;

    private void Awake()
    {
        _enemySpawner = FindObjectOfType<EnemySpawner>();
        _enemyCounter = FindObjectOfType<EnemyCounter>();
    }

    private void OnEnable()
    {
        _enemyCounter.OnCountNull += EndRound;
    }

    private void OnDisable()
    {
        _enemyCounter.OnCountNull -= EndRound;
    }

    private void Start()
    {
        StartNextRound();
    }

    private void StartNextRound()
    {
        Round++;

        // ��������� ������� ������ � ���������� � � �������
        SpawnQueue queue = new SpawnQueue(this);
        _enemySpawner.StartSpawn(queue);
    }

    private void EndRound()
    {
        // ������� ����� ����� ������ ����� ����������� ��������� �����
        new Task(EndRoundProcess());
    }

    private IEnumerator EndRoundProcess()
    {
        yield return new WaitForSeconds(_roundStartCooldown);

        // ���� ������ ������� �� ����� ��������

        _enemyCount += _enemyCountIncrement;

        StartNextRound();
    }
}
