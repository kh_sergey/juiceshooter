using UnityEngine;

public class PlayerHitReaction : MonoBehaviour
{
    //private const string ANIMATOR_DIE = "Die";

    [SerializeField] private GameObject _HUD;
    [SerializeField] private GameObject _deathMenu;

    private Pause _pause;

    private void Awake()
    {
        _pause = FindObjectOfType<Pause>();
    }

    public void Die()
    {
        ShowDeathMenu();

        _HUD.SetActive(false);
    }

    public void ShowDeathMenu()
    {
        _pause.TogglePause();

        _deathMenu.SetActive(true);
    }
}
