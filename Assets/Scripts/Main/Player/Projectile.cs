using UnityEngine;
using EpicToonFX;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float _projectileDamage;
    [SerializeField] private float _projectileSplashRadius;
    [SerializeField] private float _projectileFireDamage;
    [SerializeField] private float _projectileFireTime;
    [SerializeField] private float _projectileFireDamageTimePeriod;
    [SerializeField] [Range(0, 1)] private float _slowMovementModifier = 0.5f; // %
    [SerializeField] private float _slowMovementTime = 2.5f;

    [SerializeField] private LayerMask _enemyLayerMask;

    private GameObject _projectileSplashObject;

    private void Awake()
    {
        _projectileSplashObject = GetComponent<ETFXProjectileScript>().impactParticle;
    }

    private void Start()
    {
        SplashRadiusChanger _radiusChanger = _projectileSplashObject.GetComponentInChildren<SplashRadiusChanger>();
        _radiusChanger.ChangeSplashObjectScale(_projectileSplashRadius);
    }

    public void Explode()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, _projectileSplashRadius, _enemyLayerMask);

        foreach (Collider hitCollider in hitColliders)
        {
            Damage(hitCollider.GetComponent<EnemyHealth>());
        }
    }

    private void Damage(EnemyHealth enemy)
    {
        enemy.Damage(_projectileDamage, false);
        EnemyStatusEffects enemyStatusEffects = enemy.GetComponent<EnemyStatusEffects>();
        enemyStatusEffects.DamagePerPeriod(_projectileFireDamage, _projectileFireTime, _projectileFireDamageTimePeriod);
        enemyStatusEffects.SlowMovement(_slowMovementModifier, _slowMovementTime);
    }
}
