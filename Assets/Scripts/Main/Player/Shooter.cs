using UnityEngine;

public abstract class Shooter : MonoBehaviour, ISelectable, IUpgradable, IUnlockable
{
    public bool IsUnlocked { get; protected set; } = false;

    protected PlayerInput _playerInput;

    private Outline _outline;
    private ShooterSelector _shooterController;

    public abstract void SetUpgradeLevel();

    protected virtual void Awake()
    {
        _playerInput = FindObjectOfType<PlayerInput>();
        _outline = GetComponentInChildren<Outline>();
        _shooterController = GetComponentInParent<ShooterSelector>();
    }

    private void ToggleOutline(bool state)
    {
        _outline.enabled = state;
    }

    public virtual void Select()
    {
        _shooterController.SelectShooter(this);
        ToggleOutline(true);
    }

    public virtual void Unselect()
    {
        ToggleOutline(false);
    }

    public virtual void Unlock()
    {
        IsUnlocked = true;
    }
}
