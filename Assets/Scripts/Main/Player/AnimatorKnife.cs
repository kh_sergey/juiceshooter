using System.Collections;
using UnityEngine;

public class AnimatorKnife : MonoBehaviour
{
    private const string ANIMATOR_CUT = "Cut";

    [SerializeField] private float _maxKnifeXAnimationSpeed = 5f;
    [SerializeField] private float _knifeXAnimationSpeedIncrementPerCut = 1f;
    [SerializeField] private float _knifeXAnimationSpeedDecrementValue;
    [SerializeField] private float _timeBeforeDisablingKnife;

    [SerializeField] private GameObject _knifeModel;
    [SerializeField] private Animator _knifeXAnimator;
    [SerializeField] private Animator _knifeYAnimator;

    private Task _disableKnifeProcess;

    private float _knifeXAnimationSpeed;

    private void Start()
    {
        _knifeXAnimator.speed = 0f;
    }

    private void FixedUpdate()
    {
        if (_knifeXAnimationSpeed > 0f)
        {
            DecreaseKnifeXAnimationSpeed(_knifeXAnimationSpeedDecrementValue);
        }
    }

    public void MakeCut()
    {
        ActivateKnife();

        IncreaseKnifeXAnimationSpeed(_knifeXAnimationSpeedIncrementPerCut);

        _knifeYAnimator.SetTrigger(ANIMATOR_CUT);

        DisableKnife(); // ����� ������� ��� ���������� ���� � ��������� ���������
    }

    private void IncreaseKnifeXAnimationSpeed(float value)
    {
        _knifeXAnimationSpeed += value;

        if (_knifeXAnimationSpeed > _maxKnifeXAnimationSpeed)
        {
            _knifeXAnimationSpeed = _maxKnifeXAnimationSpeed;
        }

        _knifeXAnimator.speed = _knifeXAnimationSpeed;
    }

    private void DecreaseKnifeXAnimationSpeed(float value)
    {
        _knifeXAnimationSpeed -= value;

        if (_knifeXAnimationSpeed < 0)
        {
            _knifeXAnimationSpeed = 0f;
        }

        _knifeXAnimator.speed = _knifeXAnimationSpeed;
    }

    private void ActivateKnife()
    {
        _knifeModel.SetActive(true);
    }

    private void DisableKnife()
    {
        if (_disableKnifeProcess != null)
        {
            _disableKnifeProcess.Stop();
        }
        _disableKnifeProcess = new Task(DisableKnifeProcess());
    }

    private IEnumerator DisableKnifeProcess()
    {
        yield return new WaitForSeconds(_timeBeforeDisablingKnife);

        _knifeModel.SetActive(false);
    }
}
