using UnityEngine;

public class PowerUpCapacity : LiquidCapacity
{
    [SerializeField] private Outline _outline;

    protected override void UpdateJuiceVisuals(float juiceValue)
    {
        base.UpdateJuiceVisuals(juiceValue);

        if (Value == MaxValue)
        {
            _outline.enabled = true;
        }
        else
        {
            _outline.enabled = false;
        }
    }
}
