using UnityEngine;
using UnityEngine.Events;
using System;

public class Pause : MonoBehaviour
{
    public event UnityAction OnGamePaused;

    public bool IsPaused { get => _isPaused; private set => _isPaused = value; }

    private bool _isPaused;

    private void Awake()
    {
        Time.timeScale = 1;
        _isPaused = false;
    }

    public void TogglePause()
    {
        Time.timeScale = Convert.ToInt32(IsPaused);

        IsPaused = !IsPaused;

        if (IsPaused)
        {
            OnGamePaused?.Invoke();
        }
    }
}
