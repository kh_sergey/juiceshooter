using UnityEngine;

public class JuiceCapacitiesStorage : MonoBehaviour
{
    public JuiceCapacity YellowJuice { get => _yellowJuice; }
    public JuiceCapacity OrangeJuice { get => _orangeJuice; }
    public JuiceCapacity RedJuice { get => _redJuice; }

    [SerializeField] private JuiceCapacity _yellowJuice;
    [SerializeField] private JuiceCapacity _orangeJuice;
    [SerializeField] private JuiceCapacity _redJuice;
}

