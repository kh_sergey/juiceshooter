public interface IUnlockable
{
    public bool IsUnlocked { get; }

    public void Unlock();
}
