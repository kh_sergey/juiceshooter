using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    public event UnityAction OnTouchDown;
    public event UnityAction OnTouchUp;

    [SerializeField] private LayerMask _raycastLayerMask;

    private Pause _pause;

    private void Awake()
    {
        _pause = FindObjectOfType<Pause>();
    }

    private void Update()
    {
        if (!_pause.IsPaused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnTouchDown?.Invoke();
                SelectRaycastedObject();
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnTouchUp?.Invoke();
            }
        }
    }

    private void SelectRaycastedObject()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity ,_raycastLayerMask))
        {
            if (hit.collider.GetComponentInParent<ISelectable>() != null)
            {
                hit.collider.GetComponentInParent<ISelectable>().Select();
            }
        }
    }
}
