using System.Collections;
using UnityEngine;

public class JuiceShooter : Shooter
{
    public bool IsShooting { get => _isShooting; private set => _isShooting = value; }

    [SerializeField] private float _damage;
    [SerializeField] private float _shootJuiceCost;
    [SerializeField] private float _maxDamageDistance;

    [SerializeField] private ParticleSystem[] _particleSystems;
    [SerializeField] private LayerMask _raycastLayerMask;
    [SerializeField] private GameObject _particleObject;

    private GameObject _aimedEnemyObject; // ������� ��������� ������ ����������. ����� ��� �������� ��������� ������ ���������� ������� ���������� �� ������ Pointer
    private JuiceCapacity _orangeJuice;
    private EnemyEventManager _enemyEventManager;
    private Border _border;
    private EnemyPointer _pointer;

    private bool _isShooting = false;
    private bool _isParticleShooting = false;
    private Task _shootJuiceProcessTask; 

    protected override void Awake()
    {
        base.Awake();
        _orangeJuice = FindObjectOfType<JuiceCapacitiesStorage>().OrangeJuice;
        _enemyEventManager = FindObjectOfType<EnemyEventManager>();
        _border = FindObjectOfType<Border>();
        _pointer = FindObjectOfType<EnemyPointer>();
    }

    public override void Select()
    {
        if (IsUnlocked)
        {
            base.Select();
            _enemyEventManager.OnEnemySelected += _pointer.Set;
            _border.OnBorderSelected += _pointer.Clear;
            _pointer.OnPointCleared += StopShootJuice;
            _pointer.OnPointSet += TryStartShootJuice;
            _orangeJuice.OnJuiceValueChanged += TryStartShootJuice;
        }
    }

    public override void Unselect()
    {
        base.Unselect();
        _enemyEventManager.OnEnemySelected -= _pointer.Set;
        _border.OnBorderSelected -= _pointer.Clear;
        _pointer.OnPointSet -= TryStartShootJuice;
        _orangeJuice.OnJuiceValueChanged -= TryStartShootJuice;
    }

    public override void SetUpgradeLevel()
    {
        throw new System.NotImplementedException();
    }

    /*
    public override void Unlock()
    {
        throw new System.NotImplementedException();
    }
    */

    public void TryStartShootJuice()
    {
        if (_pointer.PointedEnemy != _aimedEnemyObject)
        {
            if (_orangeJuice.Value >= _shootJuiceCost)
            {
                _aimedEnemyObject = _pointer.PointedEnemy;
                _isShooting = true;

                if (_shootJuiceProcessTask != null)
                {
                    _shootJuiceProcessTask.Stop();
                }
                _shootJuiceProcessTask = new Task(ShootJuiceProcess(_pointer.PointedEnemy));

                if (!_isParticleShooting)
                {
                    foreach (ParticleSystem particleSystem in _particleSystems)
                    {
                        particleSystem.Play();
                    }
                    _isParticleShooting = true;
                }
            }
        }
    }

    private IEnumerator ShootJuiceProcess(GameObject aimedEnemyObject)
    {
        Ray ray = new Ray(_particleObject.transform.position, aimedEnemyObject.transform.position - _particleObject.transform.position);

        while (_isShooting && _orangeJuice.Value >= _shootJuiceCost)
        {
            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _raycastLayerMask))
            {
                if (hit.collider.TryGetComponent(out EnemyHealth hittedEnemy))
                {
                    UpdateParticleGunRotation(aimedEnemyObject.transform.position);
                    float damage = CalculateEnemyDamage(hittedEnemy.gameObject);

                    if (damage > 0)
                    {
                        hittedEnemy.Damage(damage, true);
                    }

                    _orangeJuice.Decrease(_shootJuiceCost);
                    yield return new WaitForFixedUpdate();
                } else
                {
                    _isShooting = false;
                }
            } else
            {
                _isShooting = false;
            }
        }

        if (_orangeJuice.Value < _shootJuiceCost)
        {
            StopShootJuice();
        }
    }

    public void UpdateParticleGunRotation(Vector3 shootDirection)
    {
        Vector3 direction = shootDirection - _particleObject.transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        _particleObject.transform.rotation = Quaternion.Lerp(_particleObject.transform.rotation, rotation, 10 * Time.deltaTime);
    }

    private float CalculateEnemyDamage(GameObject enemyObject)
    {
        float damage = _damage;
        float distance = Vector3.Distance(enemyObject.transform.position, _particleObject.transform.position);

        if (distance >= _maxDamageDistance)
        {
            damage = 0;
        }

        return damage;
    }

    public void StopShootJuice()
    {
        _aimedEnemyObject = null;
        _isShooting = false;

        if (_isParticleShooting)
        {
            foreach (ParticleSystem particleSystem in _particleSystems)
            {
                particleSystem.Stop();
            }

            _isParticleShooting = false;
        }
    }
}
