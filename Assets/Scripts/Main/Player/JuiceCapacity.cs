using UnityEngine.Events;


public class JuiceCapacity : LiquidCapacity
{
    public event UnityAction OnJuiceValueChanged;

    public override float Value { get => base.Value; protected set { base.Value = value; OnJuiceValueChanged?.Invoke(); } }
}
