using UnityEngine;

public class LandmineCollisionDetecter : MonoBehaviour
{
    private Landmine _landmine;

    private void Awake()
    {
        _landmine = GetComponentInParent<Landmine>();
    }

    private void OnTriggerEnter(Collider collider)
    {                                                  
        if (collider.GetComponentInParent<EnemyHealth>() != null)
        {
            if (_landmine.IsActivated)
            {
                _landmine.Explode();
            }
        }
    }
}
