using UnityEngine;

public class FruitStorage : MonoBehaviour, ISelectable
{
    [SerializeField] private GameObject _pickableFruit;
    [SerializeField] private Fruit _holdingFruit;

    private FruitHolder _fruitHolder;

    private void Awake()
    {
        _fruitHolder = FindObjectOfType<FruitHolder>();

        _fruitHolder.OnFruitReleased += Restore;
    }

    public void Select()
    {
        PickUp();
    }

    public void PickUp()
    {
        _fruitHolder.TakeFruit(_holdingFruit);

        _pickableFruit.SetActive(false);
    }

    public void Restore()
    {
        _pickableFruit.SetActive(true);
    }
}
