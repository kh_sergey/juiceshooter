using UnityEngine;

public class MinesShooter : Shooter
{
    private const string GROUND_LAYER = "Ground";

    [SerializeField] private float _placeMineJuiceCost;

    [SerializeField] private GameObject _dummyLandmine; // ������������ ��� ����������� ����� ������������ ����. ������������� ��� ������������ ������.
    [SerializeField] private GameObject _dummyLandmineRadiusObject;
    [SerializeField] private GameObject _landminePrefab; // ������������ ��� ��������������� �������� ����
    [SerializeField] private Transform _spawnedLandmineParent;
    [SerializeField] private LayerMask _placingLayerMask;

    private JuiceCapacity _yellowJuice;
    private Border _border;
    private Landmine _landmineStats;

    private bool _isHolding;
    private bool _isCorrectlyPlaced;
    private Vector3 _spawnPosition;

    protected override void Awake()
    {
        base.Awake();
        _yellowJuice = FindObjectOfType<JuiceCapacitiesStorage>().YellowJuice;
        _border = FindObjectOfType<Border>();
        _landmineStats = _landminePrefab.GetComponent<Landmine>();
    }

    private void Start()
    {
        ApplyDummyLandmineExplodeRadius();
    }

    private void ApplyDummyLandmineExplodeRadius()
    {
        _dummyLandmineRadiusObject.transform.localScale = new Vector3(_landmineStats.ExplodeRadius, _landmineStats.ExplodeRadius, _landmineStats.ExplodeRadius);
    }

    private void Update()
    {
        if (_isHolding)
        {
            HoverDummyLandmine();
        }
    }

    public override void Select()
    {
        if (IsUnlocked)
        {
            base.Select();
            _border.OnBorderSelected += StartHoverDummyLandmine;
        }
    }

    public override void Unselect()
    {
        base.Unselect();
        _border.OnBorderSelected -= StartHoverDummyLandmine;
        _playerInput.OnTouchUp -= TryPlaceLandmine;
    }

    public override void SetUpgradeLevel()
    {
        throw new System.NotImplementedException();
    }

    /*
    public override void Unlock()
    {
        throw new System.NotImplementedException();
    }
    */

    private void StartHoverDummyLandmine()
    {
        if (_yellowJuice.Value >= _placeMineJuiceCost)
        {
            _isHolding = true;
            _playerInput.OnTouchUp += TryPlaceLandmine;
        }
    }

    private void HoverDummyLandmine()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _placingLayerMask))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer(GROUND_LAYER))
            {
                _isCorrectlyPlaced = true;
                _dummyLandmine.gameObject.SetActive(true);
                _dummyLandmine.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
                _spawnPosition = hit.point;
            }
            else
            {
                _isCorrectlyPlaced = false;
                _dummyLandmine.gameObject.SetActive(false);
                _spawnPosition = Vector3.zero;
            }
        }
    }

    private void TryPlaceLandmine()
    {
        _isHolding = false;
        _dummyLandmine.gameObject.SetActive(false);

        if (_isCorrectlyPlaced && _yellowJuice.Value >= _placeMineJuiceCost) // ������ �������� �� ���������� ���� �� ������ ������
        {
            GameObject spawnedLandmine = Instantiate(_landminePrefab, _spawnPosition, Quaternion.identity);
            spawnedLandmine.transform.parent = _spawnedLandmineParent;

            _yellowJuice.Decrease(_placeMineJuiceCost);
        }

        _playerInput.OnTouchUp -= TryPlaceLandmine;
    }
}
