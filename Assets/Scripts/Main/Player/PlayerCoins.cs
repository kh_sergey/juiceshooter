using UnityEngine;
using UnityEngine.Events;

public class PlayerCoins : MonoBehaviour
{
    public event UnityAction OnCoinsValueChanged;

    public int Value { get => _value; private set { _value = value; OnCoinsValueChanged?.Invoke(); } }

    private int _value;

    private void Start()
    {
        // ������ ������� ����� ������ �������� �� PlayerPrefs, �� ���� ��� 0

        Value = 0;
    }

    public void Increase(int value)
    {
        Value += value;
    }

    public void Decrease(int value)
    {
        Value -= value;
    }
}
