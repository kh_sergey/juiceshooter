using UnityEngine;
using UnityEngine.Events;

public class FruitHolder : MonoBehaviour
{
    public event UnityAction OnFruitReleased;

    [SerializeField] private LayerMask _cuttingBoardRaycastLayerMask;
    [SerializeField] private LayerMask _hoverRaycastLayerMask;

    private bool _isHolding = false;

    private Camera _mainCamera;
    private CuttingBoard _cuttingBoard;
    private PlayerInput _playerInput;
    private Fruit _holdingFruit;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _cuttingBoard = FindObjectOfType<CuttingBoard>();
        _playerInput = FindObjectOfType<PlayerInput>();
    }

    private void Update()
    {
        if (_isHolding)
        {
            HoverHoldingFruit();
        }
    }

    private void HoverHoldingFruit()
    {
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _hoverRaycastLayerMask)) // ������ ���������� ������� �� ������� �����, ������� �� layerMask
        {
            _holdingFruit.gameObject.transform.position = hit.point + _holdingFruit.HoverOffset;
        }
    }

    public void ReleaseFruit()
    {
        _isHolding = false;
        _holdingFruit.gameObject.SetActive(false);
        OnFruitReleased.Invoke();

        if (CheckCorrectFruitPlacement())
        {
            _cuttingBoard.PutFruitOnTable(_holdingFruit);
        }

        _playerInput.OnTouchUp -= ReleaseFruit;
    }

    private bool CheckCorrectFruitPlacement()
    {
        bool result = false;

        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, _cuttingBoardRaycastLayerMask))
        {
            if (hit.collider.GetComponentInParent<CuttingBoard>())
            {
                result = true;
            }
        }

        return result;
    }

    public void TakeFruit(Fruit holdingFruit)
    {
        _holdingFruit = holdingFruit;

        _holdingFruit.gameObject.SetActive(true);
        _isHolding = true;
        _playerInput.OnTouchUp += ReleaseFruit;
    }
}
